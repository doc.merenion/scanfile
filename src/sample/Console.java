package sample;

import sample.folderParser.FolderParser;
import sample.folderParser.FolderParserAllFiles;
import sample.readingFile.Reader;
import sample.readingFile.TxtFormatReaderWithUtf8;
import java.io.File;
import java.util.Scanner;

public class Console {
    private FolderParser folderParser = new FolderParserAllFiles();
    private Reader reader = new TxtFormatReaderWithUtf8();

    public void consoleInput (){
        System.out.println("Введите путь к папке или файлу");
        String path = (new Scanner(System.in)).next();
        File file = new File(path);
        if (isFile(file))
            log("Содержимое текстового файла:\n"+reader.readFile(file));
        else{
            log("Содержимое папки:");
            for (File file1:folderParser.scanFolder(path))
            log(file1.getName());
        }
        log("__________________________");
        consoleInput();
    }

    private boolean isFile (File file){
        return file.listFiles() == null;
    }

    public static void log(String message){
        System.out.println(message);
    }

}
