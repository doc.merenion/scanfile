package sample.folderParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FloderParserOnlyTxt implements FolderParser {
    @Override
    public List<File> scanFolder(String nameFolder) {
        File fileFilder = new File(nameFolder);
        List<File> filesWithTxt = new ArrayList<>();
        for (File file: Objects.requireNonNull(fileFilder.listFiles()))
            if (Objects.equals(getType(file.getName()), ".txt"))
                filesWithTxt.add(file);
        return filesWithTxt;
    }

    private static String getType (String mystr) {
        int index = mystr.indexOf('.');
        return index == -1? null : mystr.substring(index);
    }
}
