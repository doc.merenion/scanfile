package sample.folderParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public interface FolderParser {
    List<File> scanFolder (String nameFolder);
}
