package sample.folderParser;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class FolderParserAllFiles implements FolderParser{
    @Override
    public List<File> scanFolder(String nameFolder) {
        File fileFilder = new File(nameFolder);
        List<File> files = Arrays.asList(Objects.requireNonNull(fileFilder.listFiles()));
        return files;
    }
}
