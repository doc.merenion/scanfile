package sample.readingFile;

import java.io.File;

public interface Reader {
    String readFile (File file);
}
