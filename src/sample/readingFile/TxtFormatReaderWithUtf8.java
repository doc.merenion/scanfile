package sample.readingFile;

import sample.Console;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class TxtFormatReaderWithUtf8 implements Reader{

    @Override
    public String readFile(File file) {
        StringBuilder lines = new StringBuilder();
        try {
            Scanner scanner = new Scanner(file);
            do {
                lines.append(scanner.nextLine()+"\n");
            } while (scanner.hasNextLine());
        } catch (FileNotFoundException | NoSuchElementException e) {
            Console.log("File cannot be read.\n" +
                    "UTF-8 encoding is not allowed, or file is empty.");
        }
        return lines.toString();
    }
}
